var angular = require('angular');

var dependencies = [];

/*******************************************
 *         NG-MODULES DEPENDENCIES         *
 *******************************************/
// Adding Angular-Material dependencies (JS & CSS)
var angularMaterial = require('angular-material');
require('angular-material/angular-material.css');
dependencies.push(angularMaterial);

// Adding Angular-Material-Icons dependency
require('angular-material-icons');
require('angular-material-icons/angular-material-icons.css');
dependencies.push('ngMdIcons');

// Adding Angular-Block-UI dependencies
require('angular-block-ui/dist/angular-block-ui.js');
require('angular-block-ui/dist/angular-block-ui.css');
dependencies.push('blockUI');

// Adding Angular Chart.js dependencies
require('angular-chart.js');
// require('angular-chart.js/dist/angular-chart.css');
dependencies.push('chart.js');

/*******************************************
 *              APPLICATION                *
 *******************************************/
var app = angular
    .module('streamroot', dependencies)
    .config(require('./config'));

// Requiring Components
var requireComponents = require.context(
    './components',
    true,
    /\.js*/
);

requireComponents.keys().forEach(function(key) {
    requireComponents(key)(app);
});

// Requiring Services
var requireServices = require.context(
    './services',
    true,
    /\.js*/
);

requireServices.keys().forEach(function(key) {
    requireServices(key)(app);
});
