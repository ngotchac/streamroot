var influent = require('influent/dist/influent.js');

InfluxDbService.$inject = ['$rootScope'];

function InfluxDbService($rootScope) {

    var selfClient,
        dbValues = {};

    return {
        init: init,
        query: query,
        values: () => dbValues
    };

    /**
     * Return a Promise resolved when client is ready.
     */
    function init(protocol, host, port, database) {
        return influent
            .createHttpClient({
                server: [
                    {
                        protocol: protocol,
                        host: host,
                        port: port
                    }
                ],
                username: '',
                password: '',

                database:database
            })
            .then(function(client) {
                selfClient = client;
                return GetDbValues(client);
            })
            .then(function() {
                $rootScope.influxReady = true;
            });
    }

    /**
     * Execute the given Query, return the result as a Promise.
     *
     * @param  {String} stmt
     * @return {Promise}
     */
    function query(stmt) {
        if (!selfClient) throw new Error('Client no initialized.');

        return selfClient
            .query(stmt);
    }

    /**
     * Get the Values of the isp, browser and stream in the DB
     * (the Distinct values)
     */
    function GetDbValues(client) {
        return client
            .query('SHOW TAG VALUES WITH key IN (isp, browser, stream)')
            .then(function(result) {
                var results = result.results[0].series.reduce(function(cur, e) {
                    var name = e.columns[0];
                    cur[name] = e.values.map(a => a[0]);

                    return cur;
                }, {});

                dbValues = results;

                console.log('DB Values', results);
            });
    }
}

module.exports = function(app) {
    app.factory('InfluxDb', InfluxDbService);
}