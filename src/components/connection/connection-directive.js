// Import HTML Template & CSS
var template = require('./connection-template.html');

ConnectionComponent.$inject = ['InfluxDb'];

function ConnectionComponent(InfluxDb) {
    return {
        restrict: 'E',
        template: template,
        scope: {
            reload: '='
        },

        controller: function($scope) {
            $scope.influxdb =  {
                protocol: 'http',
                host: 'data-test.cloudapp.net',
                port: 8086,
                database: 'testdata'
            };

            $scope.connect = function() {
                var url = [
                        $scope.influxdb.protocol,
                        '://',
                        $scope.influxdb.host,
                        ':',
                        $scope.influxdb.port,
                        '/',
                        $scope.influxdb.database
                    ].join('');

                console.log('Connecting to:', url);

                InfluxDb
                    .init(
                        $scope.influxdb.protocol,
                        $scope.influxdb.host,
                        $scope.influxdb.port,
                        $scope.influxdb.database
                    )
                    .then(function() {
                        $scope.reload()
                    });
            };

            // For Dev
            // $scope.connect();
        }
    };
}

module.exports = function(app) {
    app.directive('connection', ConnectionComponent);
};
