// Import HTML Template & CSS
var template = require('./data-miner-template.html');
var style = require('./data-miner-style.less');

DataMinerComponent.$inject = ['InfluxDb'];

function DataMinerComponent(InfluxDb) {
    return {
        restrict: 'E',
        template: template,
        scope: {},

        controller: function($scope) {
            $scope.className = style.className;
            $scope.dbValues = InfluxDb.values();

            $scope.filters = {
                stream: {
                    items: [].concat($scope.dbValues.stream),
                    selected: [].concat($scope.dbValues.stream)
                },
                isp: {
                    items: [].concat($scope.dbValues.isp),
                    selected: [].concat($scope.dbValues.isp)
                },
                browser: {
                    items: [].concat($scope.dbValues.browser),
                    selected: [].concat($scope.dbValues.browser)
                },
                groupBy: {
                    values: ['stream', 'browser', 'isp'],
                    selected: 'stream'
                }
            };

            $scope.toggle = function (item, list) {
                var idx = list.indexOf(item);
                if (idx > -1) {
                    list.splice(idx, 1);
                } else {
                    list.push(item);
                }
            };

            $scope.exists = function (item, list) {
                return list.indexOf(item) > -1;
            };

            $scope.isIndeterminate = function(filter) {
                var is = (
                    filter.selected.length !== 0 &&
                    filter.selected.length !== filter.items.length
                );

                return is;
            };

            $scope.isChecked = function(filter) {
                return filter.selected.length === filter.items.length;
            };

            $scope.toggleAll = function(filter) {
                if (filter.selected.length === filter.items.length) {
                    filter.selected = [];
                } else if (filter.selected.length === 0 || filter.selected.length > 0) {
                    filter.selected = filter.items.slice(0);
                }
            };

            $scope.query = function() {
                var browsers = $scope.filters.browser.selected;
                var isps = $scope.filters.isp.selected;
                var streams = $scope.filters.stream.selected;

                var groupBy = $scope.filters.groupBy.selected;

                buildChart(browsers, isps, streams, groupBy);
            };

            function buildChart(browsers, isps, streams, groupBy) {
                var whereClause = '(' + [
                    { name: 'browser', values: browsers },
                    { name: 'isp', values: isps },
                    { name: 'stream', values: streams }
                ].map(function(datum) {
                    return datum.values
                        .map(e => datum.name+'=\''+e+'\'').join(' OR ');
                }).join(')\n AND \n(') + ')';

                InfluxDb
                    .query(`
                        SELECT
                            100*SUM(p2p) / (SUM(p2p) + SUM(cdn)) AS p2p
                        FROM measures
                        WHERE
                            ${whereClause}
                        GROUP BY ${groupBy}
                    `)
                    .then(function(result) {
                        var data = result.results[0].series.reduce(function(cur, e) {
                            var col = e.columns.indexOf('p2p');
                            var val = e.values[0][col];
                            var tags = e.tags;

                            Object.keys(tags).forEach(function(tag) {
                                if (!cur[tag]) cur[tag] = {};
                                cur[tag][tags[tag]] = val;
                            });

                            return cur;
                        }, {});

                        console.log(data);

                        var prefix = groupBy[0].toUpperCase() + groupBy.slice(1);
                        $scope.labels = Object.keys(data[groupBy])
                            .map(e => prefix + ' ' + e);
                        $scope.series = ['P2P'];

                        $scope.data = [
                            Object.keys(data[groupBy])
                                .map(e => Math.round(data[groupBy][e]*100)/100)
                        ];

                        $scope.dataReady = true;

                        $scope.$digest();
                    });
            }
        }
    };
}

module.exports = function(app) {
    app.directive('dataminer', DataMinerComponent);
};
