// Import HTML Template & CSS
var template = require('./app-template.html');
var style = require('./app-style.less');

AppComponent.$inject = ['InfluxDb', '$window'];

function AppComponent(InfluxDb, $window) {
    return {
        restrict: 'E',
        template: template,
        scope: {},

        controller: function($scope) {
            $scope.className = style.className;
            $scope.reload = function() {
                $window.setTimeout($scope.$digest.bind($scope), 50);
            };
        }
    };
}

module.exports = function(app) {
    app.directive('app', AppComponent);
};
