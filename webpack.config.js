var path = require('path'),
    webpack = require('webpack'),
    HtmlWebpackPlugin = require('html-webpack-plugin');

var production = process.env.NODE_ENV === 'production';

var plugins = [
    new HtmlWebpackPlugin({
        title: 'Streamroot - Data Mining',
        template: './src/index.html',
        inject: 'body',
        hash: true
    })
];

if (production) {
    plugins.push(new webpack.optimize.UglifyJsPlugin({
        compress: {
            warnings: false,
        },
        sourceMap: false,
        mangle: false
    }));
}

var config = {
    entry: './src/index.js',
    output: {
        path: path.join(__dirname, 'public'),
        filename: 'main.js'
    },
    plugins: plugins,
    module: {
        loaders: [
            { test: /\.js$/, exclude: /(node_modules)/, loader: 'babel' },
            { test: /\.html/, loader: 'html', exclude: /index\.html$/ },
            { test: /\.css$/, loader: 'style!css' },
            { test: /\.less/, loader: 'style!css!less' }
        ]
    }
};

if (!production) {
    config.devtool = '#eval-source-map';
}

module.exports = config;
