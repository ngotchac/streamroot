# Streamroot - Data-Mining

## Dependencies

To install the dependencies, just run:
```bash
$> npm install
```

## Run

To run the Web App:
```bash
$> npm start
```

It should be accessible [here](http://localhost:8080/webpack-dev-server/).
